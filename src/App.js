import daant from './daant.png';
import cost_bar from './cost_bar.png';
import upper_bar from './upper_bar.png';


import './App.css';
import { Link } from 'react-router-dom';

function App() {
  return (
    <div>
        <div class="container">
        <img src={daant} alt="pic1" id="pic1" />

        <img src={upper_bar} alt="upper bar" id="bar" />
      </div>
      <h2 class="top-part">Dental Shelter</h2>

      <br />
      <div class="tables">
        <table class="t1">
          <thead>
            <tr>
              <th class="col1" colspan="3" rowspan="2">
                Preliminary Detection
              </th>
              <th class="col1" colspan="7" rowspan="2">
                Receding Gums
                <i class="fa fa-video-camera fa-lg" style={{color: "blue"}}></i>
              </th>
            </tr>
            <tr></tr>
          </thead>
          <tbody>
            <tr>
              <td class="col2" colspan="3" rowspan="2">Cause</td>
              <td class="col3" colspan="7" rowspan="2">
                Tartar builds under your teeth which causes bone loss <br />and
                your gums to recede.
              </td>
            </tr>
            <tr></tr>
          </tbody>
        </table>
        <br />
        <table class="t2">
          <thead>
            <tr>
              <td class="col4" colspan="3" rowspan="2">Self Help</td>
              <td class="col4" colspan="7" rowspan="2">
                Enhance oral hygiene with proper brushes and flossing<br />to
                eliminate bacteria from the gum. <a href="">Read More</a>
              </td>
            </tr>
            <tr></tr>
          </thead>
        </table>
        <br />
        <table class="t3">
          <thead>
            <tr>
              <th class="col5" colspan="3" rowspan="2">
                Dentist <i class="fa fa-map fa-lg"></i>
              </th>
              <th class="col5" colspan="7" rowspan="2">
                Deep Cleaning | Tooth Extraction
              </th>
            </tr>
            <tr></tr>
          </thead>
          <tbody>
            <tr>
              <td class="col6" colspan="3" rowspan="2">Cost</td>
              <td class="col6" colspan="7" rowspan="2">
                <img src={cost_bar} alt="cost bar" />
              </td>
            </tr>
            <tr></tr>
          </tbody>
        </table>
        <br />
        <Link to={'details'} >
        <button style={{fontSize: "24px"}}>
          Proceed <i class="fs fa-less-than"></i>
        </button>
        </Link>
      </div>
      <br />
      <footer id="dk-footer" class="dk-footer">
        <div class="container">
          <div class="col-lg-6">
            <div class="copyright-menu">
              <ul>
                <li>
                  <a  className='lopaaaa'href="#">Terms</a>
                </li>

                <li>
                  <a className='lopaaaa' href="#">About</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
}

export default App;
